Translation of the website
==========================

You can translate the interface of the website. This will add the language pill on the frontpage of the website. The number of text to translate isn't very long; around 300 lines of keywords and small paragraphs. 

![](https://www.peppercarrot.com/data/images/lab/2015-03-02_tuto-translation/2015-03-01_g_about-website.jpg)  

## The website repository

The file to translate the Pepper&Carrot website are all in **/themes/peppercarrot-theme_v2/lang** of this [the "website" repository](https://framagit.org/peppercarrot/website "Git repository").

To translate just follow the same workflow than for the [webcomic translation](translation_03_webcomic.md). The only difference: you'll duplicate the en.php file and rename it to your language code. Then edit it with a text-editor (easier with a text-editor that support the PHP highlight code color while editing: Notepad++, Geany, Kate, Gedit, Mousepad, etc...). 

**Understanding the format:**

1. For a typical line like this one:

`` 'FOLLOW'        =>  'Follow Pepper&amp;Carrot on :', ``

2. The first part `` 'FOLLOW' ``  is the unique ID, do not translate it.

3. The arrow `` => `` links the previous field with the next one (note, you can add any amount of spacebar characters between the fields (eg. if you want to align all `` => `` in your file)).

4. Last field is ``'Follow Pepper&amp;Carrot on :'`` use HTML encapsuled inside ``'`` symbols. It's the content to translate. If you need a special character like the one ``&``, use the charset ``&amp;``, and if you need to use ``'``, you'll need to escape it with a backslash ``\'`` . Except that, the website is encoded in UTF8, so pretty tolerant to all euro-latino-accents, and other unicode compatible languages.

5. Last character in the line is ``,`` and mean the line is over.
  

