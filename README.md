# Introduction:

### A documentation for contributors by contributors

This documentation is the technical reference for the contributors of Pepper&Carrot:
- **Translators**
- **Developers**
- **Creators of derivations**

The purpose of this wiki is to centralize all the technical information for you to be independent. I hope you'll find it useful.

**Dynamic documentation**

This documentation is dynamic: everyone who has an account on Framagit can propose changes to this wiki by opening a Merge Request on the [Git repository here](https://framagit.org/peppercarrot/documentation). Trusted people might receive developer permissions, they can edit the repository directly.

For more information about how to register on Framagit and get developer permissions, read [Method using a Web browser](index.php?en/static14/documentation&page=040_-_Method_using_a_Web_browser), chapter 3, part a 'Connect with us'.

License: terms and conditions
=============================

By submitting any content to the Pepper&Carrot [webcomics repository on Framagit](https://framagit.org/peppercarrot/webcomics), such as new translations, improvements to translations, issues, or other content, (“Your Content”), you accept that you release it under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License (“CC BY 4.0”). You acknowledge that you understand this license and you permit everyone to use Your Content under this license. You warrant that you have the right to grant this permission and you understand that under the terms of the CC BY 4.0 license you cannot later revoke it.

You are of course still free to make derivatives and translations of Pepper&Carrot and distribute them under other licenses, so long as you comply with the applicable licenses. But if you wish to have them included in this repository or on [the official website](https://www.peppercarrot.com/), you must release them under CC BY 4.0.

(This does not apply to files in the `fonts/` directory, which are released under their own separate license agreements.)

All the attributions are listed on the [AUTHOR.md](https://framagit.org/peppercarrot/webcomics/blob/master/AUTHORS.md) files at the root of the repository and also under the [Author](https://www.peppercarrot.com/en/static7/author) menu on [the main website](https://www.peppercarrot.com/ "peppercarrot.com").
