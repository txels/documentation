How to align and distribute text
================================

## Center alignment of titles

![English episode header](https://www.peppercarrot.com/data/documentation/medias/img/center-align1.png "Example for English episode header")

On some episode titles and thumbnails, the original text has not been defined as center aligned.
So, since your translation will have a different width, the text layout will not be clean any more:

![Translated episode header](https://www.peppercarrot.com/data/documentation/medias/img/center-align2.png "Example for translated episode header")

On some thumbnails, this was done on purpose in order not to have the text run across some details in the image.
However, you will want to fix this in general.

The first thing we do is to create a big rectangle as an anchor:

![Rectangle tool](https://www.peppercarrot.com/data/documentation/medias/img/center-align3.png "Selecting the rectangle tool")

Make sure to snap to "Corner to Corner" on the page:

![Corner to Corner](https://www.peppercarrot.com/data/documentation/medias/img/center-align4.png "Corner to Corner rectangle")

Use the _Shift_ key and your mouse to select the rectangle and your text. Then select "Object" -> "Align and Distribute..."

![Align and Distribute menu entry](https://www.peppercarrot.com/data/documentation/medias/img/center-align5.png "Selecting the Align and Distribute entry")

Click on the "Center on vertical axis" icon

![Center on vertical axis](https://www.peppercarrot.com/data/documentation/medias/img/center-align6.png "Center on vertical axis icon")

Inkscape has aligned the text for us now. Mark the rectangle only with your mouse and delete it by hitting the _Del_ key on your keyboard.


## Consistent line distance

Sometimes, text will flow across multiple interconnected speechbubbles and we will want to keep the layout consistent. Here's an example:

![English speechbubbles](https://www.peppercarrot.com/data/documentation/medias/img/distribute1.png "Example for English speechbubbles")

If your translation has more or less lines than the original, the layout will not work any more:

![Translated speechbubbles](https://www.peppercarrot.com/data/documentation/medias/img/distribute2.png "Example for translated speechbubbles")

We will want even gaps between the lines (of course, you would also have made the middle bubble less high and moved the text closer together, but we skip this for now).

Use the _Shift_ key and your mouse to select the all text objects that you want to distribute evenly. Then select "Object" -> "Align and Distribute..."

![Align and Distribute menu entry](https://www.peppercarrot.com/data/documentation/medias/img/distribute3.png "Selecting the Align and Distribute entry")


Click on the "Make vertical gaps between objects equal" icon:

![Make vertical gaps between objects equal](https://www.peppercarrot.com/data/documentation/medias/img/distribute4.png "Make vertical gaps between objects equal icon")

The vertical positioning has now been fixed while keeping the horizontal positions as they are.

![Done](https://www.peppercarrot.com/data/documentation/medias/img/done.png "Pepper blowing her wand") Done!
