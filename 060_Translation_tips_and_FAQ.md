Translating characters names
============================

**Is it possible to change the characters' names for a translation?**

Sure, you can. For the French translation, I kept the english 'Carrot' because the French name "Carotte" is feminine. I kept 'Pepper' and didn't use the translation 'Poivre' or 'Poivron' because it sounds like an insult French-speakers may throw at a drunkard, 'Poivrot!'. 

**The reference file**

To keep track of all international names while you translate the episodes, you can fill this spreadsheet ["translation-names-references.fods"](https://www.peppercarrot.com/0_sources/translation-names-references.fods) at the root of the webcomics repository. It's a \*.fods file you can open with [LibreOffice Calc](https://www.libreoffice.org/). 

You can save your modification to this file and send it back on the webcomics repository the same way as you would do for proposing a translation. More information on editing and uploading a file on the ["Method using a Web browser"](https://www.peppercarrot.com/static14/documentation&page=040_-_Method_using_a_Web_browser) page.

Adapting the geometry of speech-bubbles
=======================================

**Padding**

Keep a good looking padding distance around the text; a text touching the edge of the speechbubble doesn't look good.

**Adapting the Shape**

The arrow tool on the top of the vertical toolbar of Inkscape will allow you to deform the edges of the speechbubble. You can adapt the geometry of the speechbubble to match your translation (sometime translation needs larger speechbubble, or smaller).

**Mirroring**

Sometimes, a speechbubble would fit its text or surroundings more easily if it was mirrored vertically or horizontally, like the bottom speechbubble in this example:

![Example for mirrored speech bubble](https://www.peppercarrot.com/data/documentation/medias/img/flipped-bubble.png "Example for mirrored speech bubble")

This can be a lot faster than manually deforming the speechbubble. Here are the Inkscape buttons to do this:

![Inkscape button for flipping an object](https://www.peppercarrot.com/data/documentation/medias/img/flip-bubble.png "Inkscape button for flipping an object")


**About**

All speechbubbles are white geometric shapes made of vector point composed of two parts: the body (the bubble) and the tail. Because both element are white or use the same color and borderless they merge visually into a single shape.

Italic, Bold and text-effects
=============================

Select only the text you want to make bold, then change the font style to the bold one. This [picture](https://www.peppercarrot.com/data/images/faq/2018-07-01_bold-Lavi-fix.png) shows how to do that.

If the font you are using doesn't have a bold variant (for example the old Lavi), you can select the text and add a StrokePaint to it, and change the StrokeStyle width. Here is a [picture](https://www.peppercarrot.com/data/images/faq/2018-02-22_bold-text-faq.jpg) showing how I'm doing it.

Text positioning
================

You will sometimes need to center text horizontally or distribute it evenly

*  [Center alignment of titles](./061_Translation_tips_text-alignment.md#center-alignment-of-titles)
*  [Consistent line distance](./061_Translation_tips_text-alignment.md#consistent-line-distance)

Editing line-height of onomatopias
==================================

Line-height was sometime used on each letters of onomatopia for stylistic reasons. To edit an onomatopia with this type of effect with Inkscape you'll have to translate it as usual; it will flatten the word into a single line then you'll have to rebuild manually the various line-height for each letters as described on this gif animation by placing your cursor before the letter you want to change:

![Example for mirrored speech bubble](https://www.peppercarrot.com/data/documentation/medias/img/editing_lineheight.gif "Example of editing line-height with Inkscape")

Custom episode header
=====================

Tharinda Divakara created a tutorial about how to create a cool vector title with Pepper&Carrot style. You can [read it here](https://www.peppercarrot.com/data/images/lab/2015-02-21_Multilingue-SVG-researches/2015-03-04_Sinhala-title-translation_how-to_by-Tharinda-Divakara.jpg "Tutorial about creating vector titles in Pepper&Carrot style").

Custom fonts
============

You can add [to the Pepper&Carrot Framagit repository](https://framagit.org/peppercarrot/webcomics) new fonts for your language. 

**Requirements:**

The fonts need to be published under a free license or released in the public domain.  
The following font licenses are accepted:

* Public domain fonts
* GNU/GPL fonts
* CC-0 fonts
* CC-BY fonts
* SIL Open Font License (OFL)

**Font information**

To ease the storage of the font on the project and its maintenance, it is convenient to also keep the following information in the repository:

* Author name or nickname of the font (File Fontname.COPYRIGHT)
* Link to the source website distributing the font. (File Fontname.COPYRIGHT)
* License file itself, txt version. (File Fontname.LICENSE)

For the formatting, look how it is done for [other fonts in the repository here](https://framagit.org/peppercarrot/webcomics/tree/master/fonts/Latin). 

**Where:**

Sources for finding new open fonts:

* [Font Library](https://fontlibrary.org/)
* [The League of Moveabletype](https://www.theleagueofmoveabletype.com/)
* [Fontspace](http://www.fontspace.com/category/open) with category “Open”
* [Font Squirrel](https://www.fontsquirrel.com/)

F.A.Q.
======

**Q: When I open a SVG, Inkscape prompt me with a dialog window at start-up "Convert Legacy Inkscape file", what should I do?**  
A: The one looking like looking like [that](https://www.peppercarrot.com/data/documentation/medias/img/convert-legacy-inkscape-file.jpg)? Just keep the default option "This file contains digital artwork for screen display. (Choose if unsure)" and then press the OK button.

**Q: A sentence doesn't sound good in my language, can I rewrite it?**  
A: That's ok. The most important is to keep the semantic, the feeling and the information of the story. Adding style and changing the expression of your target language is natural.

**Q: Do I need to translate all the episodes to get published?**  
A: No. Feel free to contribute within your possibilities: you can translate just one episode then try one another later. Take your time. The website is designed to fall back to English if there's no translation available for a given episode. So, you can send episode 01, then one month later send episode 03, 04, 05 in a row, then stop here if you want... or then send all the other episodes. Every contribution is welcome; thank you for your effort and contributions!

**Q: I translated already many episodes, but the language I maintain still is not appearing in the lang list on the homepage? Why**  
A: Yes, homepage (and other pages of the website) use the website translation file (a PHP file), while episodes uses their own translation files (the SVGs). If you want the button on the homepage, please [translate the website using this tutorial](https://www.peppercarrot.com/static14/documentation&page=110_Website_translation).

**Q: Can I translate fictive language, as Klingon, Dothraki, Quenya, Sindarin?**  
A: Yes and No. Yes because you are free to translate the SVG and publish them on your blog/forum as a cross-over with a fair-use "fan-art" of Copyrighted material. 
No, if you plan to get this files accepted into the main repository of Pepper&Carrot because it is not Creative Commons compatible. You can read more about it on [this thread](https://framagit.org/peppercarrot/webcomics/issues/60).

**Q: A SVG is missing in the translation folder; at episode four: E04P05.svg!**  
A: The file is indeed missing on purpose: in the comic this panel is a GIF animation and can't be translated. I had to remove the SVG for this page so the renderfarm ignore computing a translation for this specific case. A special set of rules are applied each time a GIF animation appears in the story. That's why it is a rare situation.

**Q:** **Where can I get an overview of all available translations?**  
A: We maintain [a big table](https://www.peppercarrot.com/en/static6/sources&page=translation) on the official website to get an overview of the translation efforts.


**Q:** **Where can I subscribe for notifications about new episodes that are ready to translate?**  
A: We will create an issue in GitLab when new translations are needed. 
The issue will be tagged with the red "announcement to translators" label. 
You can subscribe to the label on the [group labels page](https://framagit.org/groups/peppercarrot/-/labels).
